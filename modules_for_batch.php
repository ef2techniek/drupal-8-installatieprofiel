<?php
$forcedVersions = array('linkit' => '5');

function __ef2_load_modules(){    

    $modules = array(
        'linkit', 
        'ds',
        'imce', 
        'token', 
        'metatag',
        'schema_metatag', 
        'admin_toolbar',
        'adminimal_admin_toolbar', 
        'ctools', 
        'pathauto', 
        'simple_sitemap', 
        'field_group',
        'cacheflush',
        'contribute',
        'webform',
        'yoast_seo',
        'crop',
        'focal_point',
        'paragraphs',
        'background_image_formatter',
        'entity_reference_revisions',
        'easy_breadcrumb',
        'redirect',
        'link_class',
        'menu_block',
        'menu_link_attributes',
        'video_embed_field',
        'ip_anon',
        'editor_advanced_link'
    );
    
    foreach($modules AS $module){
        $dl = __ef2_get_download($module);
        echo $dl . " ";
    }
}

function __ef2_load_themes(){
    $modules = array(
        'adminimal_theme'
    );
    
    foreach($modules AS $module){
        $dl = __ef2_get_download($module);
        echo $dl . " ";
    }
}


function __ef2_get_download($module){
    $link = "https://updates.drupal.org/release-history/". $module ."/8.x";
    $xml = simplexml_load_file($link);
    
    if(!$xml->error || empty($xml->error)){
        
        $project = $xml; 
        $recommend_release = $project->api_version . '-' . (in_array($module, array_keys($forcedVersions)) ? $forcedVersions[$module] : $project->recommended_major);
        
        $download_link = $release_name = '';
        foreach($project->releases->release AS $release){
            if(strpos($release->version, $recommend_release) > -1 && (!isset($release->version_extra) || empty($release->version_extra))){
                $download_link = $release->download_link;
                $release_name = $recommend_release . '.' . $release->version_patch;
                break;
            }
        }
        
        if(empty($release_name)){
            $release = $project->releases->release[0];
            $release_name = $release->tag;
        }
        return $module . '-'. $release_name;
        
    }else{
        die('De module ' . $module . ' kon niet gevonden worden. ');
    }
    
}

if(filter_input(INPUT_GET, 't') == 'themes') {
    __ef2_load_themes();
}else{
    __ef2_load_modules();
}

