
#! /bin/bash

MYPATH=$(pwd)
wget https://ftp.drupal.org/files/projects/drupal-8.6.14.tar.gz
tar -xvf drupal-8.6.14.tar.gz
rm drupal-8.6.14.tar.gz
shopt -s dotglob
mv drupal-8.6.14*/* $MYPATH
rmdir drupal-8.6.14
rm -r profiles

wget https://bitbucket.org/ef2techniek/drupal-8-installatieprofiel/get/HEAD.tar.gz
tar -xvf HEAD.tar.gz
rm HEAD.tar.gz
shopt -s dotglob
mv ef2*/* $MYPATH
sleep 2
rmdir ef2*
ls

cd modules
wget https://bitbucket.org/ef2techniek/drupal-8-ef2-module/get/HEAD.tar.gz
tar -xvf HEAD.tar.gz
rm HEAD.tar.gz
shopt -s dotglob
mv ef2*/* .
sleep 2
rmdir ef2*
ls

cd ../themes
wget https://bitbucket.org/ef2techniek/thema-starterkit/get/HEAD.tar.gz
tar -xvf HEAD.tar.gz
rm HEAD.tar.gz
shopt -s dotglob
mv ef2*/* .
sleep 2
rmdir ef2*
ls

RESULT="`wget -qO- http://drupal.ef2.nl/modules_for_batch.php`"
mkdir -p ../profiles/ef2_starter_kit/modules
cd ../profiles/ef2_starter_kit/modules
array=( $RESULT )
for i in "${array[@]}"
do
	MY_FILE=$i".tar.gz"
	wget "https://ftp.drupal.org/files/projects/"$MY_FILE
	tar -xvzf $MY_FILE
	rm $MY_FILE
done

RESULT="`wget -qO- http://drupal.ef2.nl/modules_for_batch.php?t=themes`"
mkdir -p ../themes
cd ../themes
array=( $RESULT )
for i in "${array[@]}"
do
	MY_FILE=$i".tar.gz"
	wget "https://ftp.drupal.org/files/projects/"$MY_FILE
	tar -xvzf $MY_FILE
	rm $MY_FILE
done

cd ../../../

pwd

rm modules_for_batch.php
rm install.sh